console.log('Hello World')
function Pokemon (name, level) {


	// properties
	this.name = name,
	this.level = level,
	this.health = 3*level,
	this.attack = level,

	// methods
	this.tackle = function(target){

		let newHealth = target.health-this.attack

		console.log(this.name + " tackled "+target.name)
		console.log(target.name + "'s health is now reduced to " + (newHealth))


		if (newHealth <= 5) {
			this.faint(target)
		} else {
			
			target.health = newHealth
		}
		
	},
	this.faint = function(target){
			console.log(target.name + " fainted.")
		}
}



let pikachu = new Pokemon("Pikachu", 16)
let squirtle = new Pokemon("Squirtle", 8)

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)